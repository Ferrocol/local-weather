$(document).ready(function() {
    $('#submitButton').click(function() {
        var zipCode = $('#enter-zip-code').val();
        var units = $('#select-units').val();
        loadCurrentConditions(zipCode, units);
        loadFiveDayForecast(zipCode, units);
    })
})

function loadCurrentConditions(zipCode, units) {
    var tempUom, windUom;
    $.ajax ({
        type: 'GET',
        url: 'http://api.openweathermap.org/data/2.5/weather?zip=' + zipCode
            + '&units=' + units + '&APPID=4f71491fdad56a221203459597ccc899',
        success: function(weatherData, status) {
            $('#currentConditionsInCity').empty();
            $('#currentConditionsInCity').text('Current Conditions in ' + weatherData.name);
            $('#description').empty().append('<img style="display: inline;" src="http://openweathermap.org/img/w/'
                + weatherData.weather[0].icon + '.png"/>',
                '<h4 style="width: 50%; display: inline;"> ' + weatherData.weather[0].main + ': '
                + weatherData.weather[0].description + '</h4>');
            if (units == 'metric') {
                tempUom = '&#8451;';
                windUom = "meters/sec";
            } else if (units == 'imperial') {
                tempUom = '&#8457;';
                windUom = "miles/hour";
            }
            $('#details').empty().append('<h4>Temperature: ' + weatherData.main.temp + ' ' + tempUom + '</h4>',
                '<h4>Humidity: ' + weatherData.main.humidity + '%</h4>',
                '<h4>Wind: ' + weatherData.wind.speed + ' ' + windUom + '</h4>');;
            $('#currentConditions').show();
            $('#invalidZipCode').hide();
        },
        error: function() {
            $('#invalidZipCode').show();
        }
    });
}

function loadFiveDayForecast(zipCode, units) {
    var tempUom;
    $.ajax ({
        type: 'GET',
        url: 'http://api.openweathermap.org/data/2.5/forecast?zip=' + zipCode
            + '&units=' + units + '&APPID=4f71491fdad56a221203459597ccc899',
        success: function(weatherData, status) {
            $('#days').empty();
            $('#dailyForecast').empty();
            $.each(weatherData.list, function(index, data) {
                var dateTime = new Date(data.dt_txt);
                var time = dateTime.toTimeString();
                if (time.substr(0,8) == "15:00:00") {
                    var month = getMonthName(dateTime.getMonth());
                    $('#days').append('<th>' + dateTime.getDate() + ' ' + month + '</th>');
                    $('#dailyForecast').append('<td><img src="http://openweathermap.org/img/w/'
                        + data.weather[0].icon + '.png"/><BR>' + data.weather[0].main + '<BR>'
                        + 'High: ' + data.main.temp_max + ', Low: ' + data.main.temp_min + '</td>');
                }
            });

            $('#forecast').show();
            $('#invalidZipCode').hide();
        },
        error: function() {
            $('#invalidZipCode').show();
        }
    });
}

function getMonthName(monthInt) {
    switch(monthInt) {
        case 0:
            return "January";
            break;
        case 1:
            return "February";
            break;
        case 2:
            return "March";
            break;
        case 3:
            return "April";
            break;
        case 4:
            return "May";
            break;
        case 5:
            return "June";
            break;
        case 6:
            return "July";
            break;
        case 7:
            return "August";
            break;
        case 8:
            return "September";
            break;
        case 9:
            return "October";
            break;
        case 10:
            return "November";
            break;
        case 11:
            return "December";
            break;
    }
}